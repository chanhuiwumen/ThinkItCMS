package com.thinkit.security.login;

import com.thinkit.cms.api.admin.MemberService;
import com.thinkit.cms.api.admin.UserService;
import com.thinkit.cms.dto.admin.MemberDto;
import com.thinkit.cms.dto.admin.UserDto;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LoginServiceImpl implements  LoginService {

    @Resource
    private UserService userService;

    @Resource
    private MemberService memberService;


    @Override
    public UserDto loadUserByUsername(String userAccount) {
        return userService.loadUserByUsername(userAccount);
    }

    @Override
    public MemberDto loadMemByUsername(String userAccount) {
        return memberService.loadMemUserByUsername(userAccount);
    }
}
