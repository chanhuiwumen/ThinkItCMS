package com.thinkit.security.custom;
import com.thinkit.core.constant.SecurityConstants;
import com.thinkit.security.login.VerificationExecutor;
import com.thinkit.utils.enums.UserFrom;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Map;

public class CustomUserAuthenticationBaseProvider  {

    @Autowired
    CustomUserDetailsService customUserDetailsService;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    CustomUserLoginRiskCheck customUserLoginRiskCheck;

    @Autowired
    VerificationExecutor verificationExecutor;

    protected UserDetails loadUserByUsername(Authentication authentication) throws Exception {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
        Map<String,String> details= (Map<String,String>)authentication.getDetails();
        String clientId = details.get(SecurityConstants.CLIENT_ID);
        UserFrom userFrom = UserFrom.getUserFrom(clientId);
        if(Checker.BeNull(userFrom)){
            throw new DisabledException("终端不存在!");
        }
        UserDetails userDetails= verificationExecutor.verify(username,password,userFrom);
        return userDetails;
    }

}
