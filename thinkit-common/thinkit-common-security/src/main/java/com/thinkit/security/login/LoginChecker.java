package com.thinkit.security.login;
import com.thinkit.nosql.base.BaseRedisService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public interface LoginChecker {
    /**
     * 登录查询
     * @return
     */
    UserDetails login() throws Exception;

    /**
     * 登录成功的处理事件
     * @param userDetails
     */
    void loginSuccess(UserDetails userDetails, String userAccount);

    /**
     * 登录失败的处理事件
     * @param userDetails
     */
    void loginFail(UserDetails userDetails, String userAccount);

    /**
     * 构造查询
     * @param userDetailsService
     * @param bCryptPasswordEncoder
     */
    LoginChecker build(UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder, BaseRedisService baseRedisService);
}
