package com.thinkit.core.base;
import com.thinkit.core.constant.Constants;
import com.thinkit.core.constant.SecurityConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName: BaseContextKit
 * @Author: LG
 * @Date: 2019/4/30 15:40
 * @Version: 1.0
 **/
public class BaseContextKit {

    public static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<Map<String, Object>>();

    public static void set(String key, Object value) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<String, Object>();
            threadLocal.set(map);
        }
        map.put(key, value);
    }

    public static Object get(String key) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<String, Object>();
            threadLocal.set(map);
        }
        return map.get(key) == null ? "" : map.get(key);
    }

    public static String getUserId() {
        Object value = get(SecurityConstants.USER_ID);
        String res = returnObjectValue(value);
        return res;
    }

    public static String getOrgId() {
        Object value = get(SecurityConstants.USER_ORG_ID);
        String res = returnObjectValue(value);
        return res;
    }


    public static String getUserClient() {
        Object value = get(SecurityConstants.USER_CLIENT);
        String res = returnObjectValue(value);
        return res;
    }

    public static String getAccount() {
        Object value = get(SecurityConstants.USER_ACCOUNT);
        String res = returnObjectValue(value);
        return res;
    }

    public static String getSiteId() {
        Object siteId = get(Constants.DEFAULT_SITE);
        return returnObjectValue(siteId);
    }


    public static void setUserId(String userID) {
        set(SecurityConstants.USER_ID, userID);
    }

    public static void setOrgId(String orgId) {
        set(SecurityConstants.USER_ORG_ID, orgId);
    }

    public static void setAccount(String userName) {
        set(SecurityConstants.USER_ACCOUNT, userName);
    }

    public static void setRoleSign(Set<String> roleSign) {
        set(SecurityConstants.USER_ROLE_SIGN, roleSign);
    }

    public static void setSiteId(String siteId) {
        set(Constants.DEFAULT_SITE, siteId);
    }

    public static Set<String> getRoleSign() {
        Object object= get(SecurityConstants.USER_ROLE_SIGN);
        return (Set<String>)object;
    }


    private static String returnObjectValue(Object value) {
        return value == null ? null : value.toString();
    }

    public static void remove() {
        threadLocal.remove();
    }

    public static void setUserClient(String clientId) {
        set(SecurityConstants.USER_CLIENT, clientId);
    }
}
