package com.thinkit.directive.emums;

import lombok.Getter;

public enum LangEnum {

	INTEGER("Integer","Integer类型"),

	SHORT("Short","Short类型"),

	LONG("Long","Long类型"),

	FLOAT("Float","Float类型"),

	DOUBLE("Double","Double类型"),

	BOOLEAN("Boolean","Boolean类型"),

	STRING("String","String类型"),

	DATE("Date","Date类型"),

	MAP("Map","Map类型"),

	LONG_ARRAY("LongArray","LongArray类型"),

	STRING_ARRAY("StringArray","LongArray类型"),

	INTEGER_ARRAY("IntegerArray","LongArray类型"),

	BEAN("Bean","对象类型类型");


	@Getter
	private String value;

	@Getter
	private String name;



	LangEnum(String value, String name) {
		this.name = name;
		this.value = value;
	}

	public static boolean isNumber(LangEnum langEnum){
		return langEnum.equals(INTEGER) || langEnum.equals(SHORT)||langEnum.equals(LONG)||
		langEnum.equals(FLOAT)||langEnum.equals(DOUBLE);
	}

	public static boolean isArray(LangEnum langEnum){
		return langEnum.equals(LONG_ARRAY) || langEnum.equals(STRING_ARRAY)||langEnum.equals(INTEGER_ARRAY);
	}
}
