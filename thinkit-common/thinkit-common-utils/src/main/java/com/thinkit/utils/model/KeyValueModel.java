package com.thinkit.utils.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * Created by JueYue on 2017/9/14.
 */
@Data
@Accessors(chain = true)
public class KeyValueModel implements Serializable {

    public KeyValueModel(){}


    public KeyValueModel(String code,String value){
        this.code = code;
        this.value =value;
    }


    private String code;

    private String value;


    private Boolean supportWehcat;

    private Boolean supportTops;

}
