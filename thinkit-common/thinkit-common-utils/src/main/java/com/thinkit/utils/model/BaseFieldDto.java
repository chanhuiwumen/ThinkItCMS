package com.thinkit.utils.model;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by JueYue on 2017/9/14.
 */
@Data
@Accessors(chain = true)
public class BaseFieldDto extends BaseDto {

    /**
     * 默认自带
     */
    private String defaultFieldList;

    /**
     * 扩展字段
     */
    private String extendFieldList;

    /**
     * 全部选中字段
     */
    private String allFieldList;

    /**
     * 必填字段
     */
    private String requiredFieldList;

    /**
     * 字段对应中文名称
     */
    private String fieldTextMap;

}
