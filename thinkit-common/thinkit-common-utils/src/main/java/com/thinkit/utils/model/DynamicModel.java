package com.thinkit.utils.model;

import com.thinkit.utils.enums.InputTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author LG
 * @since 2019-10-23
 */
@Data
@Accessors(chain = true)
public class DynamicModel implements Comparable<DynamicModel>{

    private static final long serialVersionUID = 1L;

    private Boolean check; // 必须勾选

    private String fieldCode; //字段code

    private String fieldName;// 字段名称

    private String fieldAliase;// 字段别名称

    private String inputType; //input 类型

    private Boolean isRequired;//是否必填

    private Boolean isSearch; //是否可搜索

    private Boolean isExtend;// 是否扩展字段

    private Object defaultValue;//默认值

    private String description ;//描述

    private Integer maxlength;//最大长度

    private Integer sort ;//排序

    private List<String> belong;

    @Override
    public int compareTo(DynamicModel o) {
        return InputTypeEnum.getSort(this.getInputType()) - InputTypeEnum.getSort(o.getInputType()) ;
    }

    public boolean equals(Object obj) {
        if (obj instanceof DynamicModel) {
            DynamicModel model = (DynamicModel) obj;
            return this.getFieldCode().equals(model.getFieldCode());
        }
        return super.equals(obj);
    }
}
