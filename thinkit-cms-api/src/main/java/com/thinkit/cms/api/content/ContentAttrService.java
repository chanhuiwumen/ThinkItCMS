package com.thinkit.cms.api.content;
import com.thinkit.cms.dto.content.ContentAttrDto;
import com.thinkit.core.base.BaseService;

/**
 * <p>
 * 内容扩展 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
public interface ContentAttrService extends BaseService<ContentAttrDto> {



}