package com.thinkit.cms.api.template;
import com.thinkit.cms.api.fragment.FragmentModelService;
import com.thinkit.core.constant.Constants;
import com.thinkit.utils.model.TreeFileModel;
import com.thinkit.utils.utils.Checker;
import com.thinkit.utils.utils.FileLoopHelp;
import java.util.HashMap;
import java.util.Map;

public class PartLoopHelp extends FileLoopHelp {

    private FragmentModelService fragmentModelService;

    private String templateId;

    private Map<String ,String> map =new HashMap<>();

    public PartLoopHelp(){

    }

    public PartLoopHelp(FragmentModelService fragmentModelService, String templateId){
      this.fragmentModelService = fragmentModelService;
      this.templateId = templateId;
      map= fragmentModelService.getMap(templateId);
    }

    @Override
    protected void filter(TreeFileModel treeFileModel) {
        if(!map.isEmpty()){
             String id = getId(treeFileModel.getTitle());
             id = map.get(id);
            if(Checker.BeNotBlank(id)){
                treeFileModel.setTitle(id+Constants.DEFAULT_HTML_SUFFIX);
            }
        }
    }

    @Override
    protected Boolean setDisableFolder() {
        return true;
    }

    @Override
    protected Boolean loadFragment() {
        return true;
    }

    @Override
    protected Boolean justLoadFragment() {
        return true;
    }

    @Override
    protected Boolean startFilter(TreeFileModel treeFileModel) {
        return treeFileModel.getIsFragment();
    }

    private String getId(String fileName){
        if(Checker.BeNotBlank(fileName)){
            String id =fileName.replace(Constants.DEFAULT_FRAGMENT_PREFIX,"").
                    replace(Constants.DEFAULT_HTML_SUFFIX,"");
            return id;
        }
        return "";
    }
}
