package com.thinkit.cms.api.site;

import com.thinkit.cms.dto.site.SiteTemplateDto;
import com.thinkit.core.base.BaseService;

/**
 * <p>
 * 站点模型表 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
public interface SiteTemplateService extends BaseService<SiteTemplateDto> {


    /**
     * 安装或者卸载模板
     * @param v
     */
    void inunstall(SiteTemplateDto v);

    /**
     * 查找默认模板路径
     * @param siteId
     * @return
     */
    String findTemplatePath(String siteId);

    /**
     * 查询默认模板
     * @param siteId
     * @return
     */
    SiteTemplateDto findTemplate(String siteId);


    /**
     * 查询默认模板Id
     * @param siteId
     * @return
     */
    String findTemplateId(String siteId);

    /**
     * 修改模板位置
     * @param id
     * @param loaction
     */
    void updateTempLocation(String id, String loaction);
}