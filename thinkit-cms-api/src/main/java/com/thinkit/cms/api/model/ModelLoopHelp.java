package com.thinkit.cms.api.model;

import com.thinkit.utils.model.TreeFileModel;
import com.thinkit.utils.utils.FileLoopHelp;

public class ModelLoopHelp extends FileLoopHelp {

    @Override
    protected void filter(TreeFileModel treeFileModel) {

    }

    @Override
    protected Boolean setDisableFolder() {
        return true;
    }

    @Override
    protected Boolean loadFragment() {
        return false;
    }

    @Override
    protected Boolean justLoadFragment() {
        return false;
    }

    @Override
    protected Boolean startFilter(TreeFileModel treeFileModel) {
        return false;
    }
}
