package com.thinkit.cms.api.job;

import com.thinkit.cms.dto.content.PublishDto;

public interface JobService {

    void jobPublish(PublishDto publishDto);
}
