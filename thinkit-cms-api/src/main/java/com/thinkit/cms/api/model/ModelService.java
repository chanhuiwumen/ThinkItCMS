package com.thinkit.cms.api.model;

import com.thinkit.cms.dto.model.FieldDto;
import com.thinkit.cms.dto.model.ModelDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.KeyValueModel;
import com.thinkit.utils.model.TreeFileModel;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LG
 * @since 2019-10-23
 */
public interface ModelService extends BaseService<FieldDto> {

    void saveModel(FieldDto v);

    void updateModel(FieldDto v);

    List<TreeFileModel> loadTemplateTree();

    Map<String, Object> loadTemplate();

    FieldDto getById(String pk);

    List<ModelDto> listModel(String categoryId);

    /**
     * 获取分类的发布模型
     * @param categoryId
     * @return
     */
    List<KeyValueModel> listPublihModel(String categoryId);

    String getFormDesign(String modelId);

    Boolean getFormHasfile(String modelId);

    ModelDto getModel(String modelId);
}
