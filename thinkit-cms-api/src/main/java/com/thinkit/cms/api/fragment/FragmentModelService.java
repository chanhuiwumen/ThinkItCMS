package com.thinkit.cms.api.fragment;
import com.thinkit.cms.dto.fragment.FragmentModelDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.DynamicModel;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 页面片段文件模型 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-02
 */
public interface FragmentModelService extends BaseService<FragmentModelDto> {

    /**
     * 创建页面片段
     * @param v
     */
    void createFile(FragmentModelDto v);

    /**
     * 根据文件名删除数据
     * @param fragmentId
     */
    void deleteFile(String fragmentId);

    /**
     * 得到页面片段id 对应 的别名 用于格式化显示别名
     * @return
     */
    Map<String,String> getMap(String templateId);

    /**
     * 获取页面片段数据信息
     * @param fileName
     * @return
     */
    FragmentModelDto getFragmentInfo(String fileName);

    /**
     * 更新片段
     * @param v
     */
    void updateFragment(FragmentModelDto v);

    /**
     * 获取片段设计字段
     * @param fileName
     * @return
     */
    List<DynamicModel> getFragmentDesign(String fileName);

    Map<String, Object> getFragmentFrom(String fileName);

    /**
     * 根据id 查询指定字段
     * @param id
     * @param fieldType 0：all_field_list 1：extend_field_list
     * @return
     */
    String getFragmentFieldListJsonStr(String id, Integer fieldType);

    /**
     * 根据 code 查询文件位置
     * @param code
     * @return
     */
    String getFragmentPathByCode(String code);

    ApiResult getPartImport(String title);
}