package com.thinkit.cms.dto.template;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.cms.dto.fragment.FragmentModelDto;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 模板表
 * </p>
 *
 * @author lg
 * @since 2020-07-23
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TemplateDto extends BaseDto {

    private static final long serialVersionUID = 1L;

    /**
     * 模板名称
     */
    @NotBlank(message = "模板名称不能为空")
    private String name;

    private String typeName;

    private Integer isDefault;

    /**
     * 分类
     */
    @NotBlank(message = "分类不能为空")
    private String typeId;

    private List<String> typeIds = new ArrayList<>();


    /**
     * 模板描述
     */
    private String templateDesc;


    private Set<String> roleSigns=new HashSet<>();

    /**
     * 售价
     */
    private String price;

    private String cover;

    private Boolean isShare;

    /**
     * 模板CODE
     */
    @NotBlank(message = "模板CODE 不能为空")
    private String templateCode;


    /**
     * 模板压缩路径
     */
    private String templateZipPath;


    /**
     * 文件夹路径
     */
    private String templateFolderPath;


    /**
     * 文件大小
     */
    private String fileSize;


    /**
     * 开发者名称
     */
    private String developerName;


    /**
     * 开发者 联系方式
     */
    private String developerContact;

    /**
     * 是否启用
     */
    private Boolean booleanUsing;


    /**
     * 静态资源名称
     */
    private String sourceFolder;

    /**
     * 分类编码
     */
    private String typeCode;

    private List<FragmentModelDto> fragmentModels;
}
