package com.thinkit.cms.dto.category;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 分类扩展模型
 * </p>
 *
 * @author lg
 * @since 2020-08-08
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryModelDto extends BaseDto {

    private static final long serialVersionUID = 1L;

        /**
        * 分类模型扩展名称
        */
        private String categoryModelName;


        /**
        * 扩展字段(json 保存)
        */
        @NotBlank(message = "请先创建字段!")
        private String extendFieldList;


        /**
        * 排序
        */
        private Integer sort;











}
