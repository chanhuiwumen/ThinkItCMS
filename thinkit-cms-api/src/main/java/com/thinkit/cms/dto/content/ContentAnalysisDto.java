package com.thinkit.cms.dto.content;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;

/**
 * <p>
 * 内容
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContentAnalysisDto extends BaseDto {

      private String name;

      private Integer month;

      private Long count;

      private Integer year;

      private Integer status ;
}
