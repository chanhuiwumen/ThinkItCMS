package com.thinkit.cms.dto.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 终端信息表
 * </p>
 *
 * @author LG
 * @since 2020-04-24
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientsDto extends BaseDto {


        private String clientId;

        /**
        * 终端描述
        */
        private String clientName;


        private String resourceIds;


        private String clientSecret;


        private String scope;


        private String authorizedGrantTypes;


        private String webServerRedirectUri;


        private String authorities;


        private Integer accessTokenValidity;


        private Integer refreshTokenValidity;


        private String additionalInformation;


        private String autoapprove;



}
