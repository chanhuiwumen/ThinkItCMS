package com.thinkit.cms.dto.license;

import lombok.Data;

@Data
public class LicenseDto {
    private String version;

    private String  organization;

    private String domain;

    private String startStopTime;

    private String authorizeDesc ;

    private String copyrightOwner;

    private String signaturer;

    private String status;
}
