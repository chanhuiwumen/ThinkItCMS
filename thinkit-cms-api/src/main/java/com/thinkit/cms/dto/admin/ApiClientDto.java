package com.thinkit.cms.dto.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author LG
 * @since 2020-05-05
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiClientDto extends BaseDto {

        /**
        * 接口资源ID
        */
        private String apiSourceId;


        private String clientId;


        /**
        * 微服务实例id
        */
        private String serviceId;


        /**
        * 0：半选中 1：全选中
        */
        private Integer halfChecked;

        private List<String> onCheckKeys;//全选中

        private List<String> halfCheckedKeys;//半选中







}
