package com.thinkit.cms.dto.content;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 内容扩展
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContentAttrDto extends BaseDto {

    private static final long serialVersionUID = 1L;

        /**
        * 主键
        */
        private String contentId;


        /**
        * 内容来源
        */
        private String origin;


        /**
        * 来源地址
        */
        private String originUrl;


        /**
        * 数据JSON
        */
        private String data;


        /**
        * 内容
        */
        private String text;


        /**
        * 字数
        */
        private Integer totalWordCount;


}
