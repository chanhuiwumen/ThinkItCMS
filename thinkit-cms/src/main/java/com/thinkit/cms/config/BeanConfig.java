package com.thinkit.cms.config;

import cn.hutool.core.util.EnumUtil;
import com.alibaba.druid.spring.boot.autoconfigure.properties.DruidStatProperties;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.thinkit.core.constant.Channel;
import com.thinkit.processor.message.MessageHandler;
import com.thinkit.utils.enums.ChannelEnum;
import com.thinkit.utils.utils.Checker;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.hibernate.validator.HibernateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;

/**
 * @ClassName: BeanConfig
 * @Author: LG
 * @Date: 2019/5/16 13:29
 * @Version: 1.0
 **/
@Configuration
public class BeanConfig {

    @Autowired
    DruidStatProperties druidStatProperties ;

    @Bean
    public DozerBeanMapperFactoryBean mapper() {
        return new DozerBeanMapperFactoryBean();
    }

    @Bean
    public PaginationInterceptor paginationInterceptor() {
         PaginationInterceptor paginationInterceptor=new PaginationInterceptor();
         paginationInterceptor.setLimit(1100);
         return paginationInterceptor;
    }


    @Bean
    public Validator validator() {
        ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class)
        .configure() // true-快速失败返回模式    false-普通模式
        .addProperty("hibernate.validator.fail_fast", "true")
        .buildValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        return validator;
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        MethodValidationPostProcessor postProcessor = new MethodValidationPostProcessor();
        /**设置validator模式为快速失败返回*/
        postProcessor.setValidator(validator());
        return postProcessor;
    }

    /**
     * 初始化监听器
     * @param connectionFactory
     * @param listenerAdapter
     * @return
     */
    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                            MessageListenerAdapter listenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        List<Object> channels= EnumUtil.getFieldValues(ChannelEnum.class,"code");
        if(Checker.BeNotEmpty(channels)){
            for(Object channel:channels){
                container.addMessageListener(listenerAdapter, new PatternTopic(channel.toString())); // new PatternTopic("这里是监听的通道的名字") 通道要和发布者发布消息的通道一致
            }
        }
        return container;
    }

    /**
     * 绑定消息监听者和接收监听的方法
     * @param messageHandler
     * @return
     */
    @Bean
    MessageListenerAdapter listenerAdapter(MessageHandler messageHandler) {
        return new MessageListenerAdapter(messageHandler, Channel.MESSAGE_METHOD);
    }
}
