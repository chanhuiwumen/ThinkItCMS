package com.thinkit.cms.config;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.utils.utils.HardWareMonitor;
import org.noggit.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Map;

@Configuration
@EnableScheduling
public class SysMonitorJob {

    @Autowired
    SimpMessagingTemplate messagingTemplate;

    @Autowired
    ContentService contentService;

    @Scheduled(cron = "0/3 * * * * ?")
    private void configureTasks() {
        Map<String,Object> hardInfo= HardWareMonitor.hardWareInfo();
        messagingTemplate.convertAndSend("/topic/hardWareMonitor", JSONUtil.toJSON(hardInfo));
    }

    @Scheduled(cron = "0/180 * * * * ?")
    private void syncViewLikes() {
          contentService.syncViewLikes();
    }
}
