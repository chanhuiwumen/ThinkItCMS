package com.thinkit.cms.service.license;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import com.thinkit.cms.api.license.LicenseService;
import com.thinkit.cms.dto.license.LicenseDto;
import com.thinkit.core.handler.CustomException;
import com.thinkit.processor.license.LicenseProperties;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.properties.ThinkItProperties;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LicenseServiceImpl implements LicenseService {

    @Autowired
    LicenseProperties licenseProperties;

    @Autowired
    ThinkItProperties thinkItProperties;

    @Override
    public PageDto<LicenseDto> page() {
        List<LicenseDto> propertiesList=new ArrayList<>();
        String[] startEnd= licenseProperties.getStartStopTime().split("~");
        if(startEnd!=null && startEnd.length>0){
            Date now = DateUtil.date();
            Date start = DateUtil.parse(startEnd[0]);
            Date end = DateUtil.parse(startEnd[1]);
            int isSatrt=DateUtil.compare(now,start);
            int isEnd=DateUtil.compare(now,end);
            long betweenDay = DateUtil.between(now, end, DateUnit.DAY);
            if(isSatrt==-1){
                licenseProperties.setStatus("未生效");
            }else if(isEnd==1){
                licenseProperties.setStatus("已过期");
            }else{
                licenseProperties.setStatus("正常");
            }
            if(betweenDay<=20){
                licenseProperties.setStatus("即将过期");
            }
        }
        LicenseDto licenseDto=new LicenseDto();
        BeanUtils.copyProperties(licenseProperties,licenseDto);
        propertiesList.add(licenseDto);
        return new PageDto<>(1,propertiesList);
    }

    @Override
    public ApiResult importLicense(MultipartFile file) {
            if(Checker.BeNotNull(file)){
                checkFile(file);
                String basePath= thinkItProperties.getLicense();
                try {
                    File filep = new File(basePath);
                    if(filep.exists()){
                        String bckLicen=basePath.replace("license.dat","")+"license_bck.dat";
                        filep.renameTo(new File(bckLicen));
                    }
                    FileUtil.writeFromStream(file.getInputStream(),new File(basePath));
                } catch (IOException e) {
                    return ApiResult.result(-1,e.getMessage());
                }
            }
            return ApiResult.result();
    }

    private void checkFile(MultipartFile file){
        String name = FileUtil.getSuffix(file.getOriginalFilename());
        if(!"dat".equals(name)){
            throw new CustomException(ApiResult.result(20033));
        }
    }
}
