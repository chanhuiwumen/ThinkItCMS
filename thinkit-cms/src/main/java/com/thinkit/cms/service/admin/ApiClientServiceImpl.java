package com.thinkit.cms.service.admin;

import com.google.common.collect.Lists;
import com.thinkit.cms.api.admin.ApiClientService;
import com.thinkit.cms.dto.admin.ApiClientDto;
import com.thinkit.cms.entity.admin.ApiClient;
import com.thinkit.cms.mapper.admin.ApiClientMapper;
import com.thinkit.core.base.BaseServiceImpl;
import com.thinkit.utils.utils.Checker;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LG
 * @since 2020-05-05
 */
@Transactional
@Service
public class ApiClientServiceImpl extends BaseServiceImpl<ApiClientDto, ApiClient, ApiClientMapper> implements ApiClientService {


    @Override
    public List<String> selectResources(String clientId) {
        return baseMapper.selectResources(clientId);
    }

    @Transactional
    @Override
    public boolean assignResource(ApiClientDto apiSourceClientDto) {
        List<String> halfCheckedKeys = apiSourceClientDto.getHalfCheckedKeys();//半选中
        List<String> onCheckKeys = apiSourceClientDto.getOnCheckKeys();//全选中
        String clientId = apiSourceClientDto.getClientId();
        deleteByClientId(clientId);
        if (onCheckKeys.size() == 1 && onCheckKeys.get(0).equals("-1")) {//全部删除
            return true;
        } else {
            List<ApiClientDto> halfList = filterList(halfCheckedKeys, clientId, 0);
            List<ApiClientDto> checkList = filterList(onCheckKeys, clientId, 1);
            List<ApiClientDto> allList = new ArrayList<>();
            allList.addAll(halfList);
            allList.addAll(checkList);
            return insertBatch(allList);
        }
    }


    private List<ApiClientDto> filterList(List<String> keys, String clientId, Integer halfChecked) {
        List<ApiClientDto> resources = new ArrayList<>();
        for (String resourceId : keys) {
            if (resourceId.equals("-1")) continue;
            ApiClientDto resource = new ApiClientDto();
            resource.setClientId(clientId);
            resource.setApiSourceId(resourceId);
            resource.setId(id());
            resource.setHalfChecked(halfChecked);
            resources.add(resource);
        }
        return resources;
    }

    @Override
    public boolean deleteByClientId(String clientId) {
        Map<String, Object> param = new HashMap<>(16);
        param.put("client_id", clientId);
        return removeByMap(param);
    }

    @Override
    public boolean deleteByResourceId(String resourceId) {
        Map<String, Object> param = new HashMap<>(16);
        param.put("api_source_id", resourceId);
        return removeByMap(param);
    }

    @Override
    public List<ApiClientDto> getsByApiId(String parentId) {
        Map<String, Object> param = new HashMap<>(16);
        param.put("api_source_id", parentId);
        List<ApiClient> sourceClients=baseMapper.selectByMap(param);
        return Checker.BeNotEmpty(sourceClients)?T2DList(sourceClients): Lists.newArrayList();
    }

}
