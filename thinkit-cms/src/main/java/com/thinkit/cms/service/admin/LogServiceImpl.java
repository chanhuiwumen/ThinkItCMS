package com.thinkit.cms.service.admin;//package com.thinkit.admin.service.impl;
//import cn.hutool.core.date.DateField;
//import cn.hutool.core.date.DateUtil;
//import com.alibaba.fastjson.JSON;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.ltzk.admin.api.api.system.LogService;
//import com.ltzk.admin.api.dto.system.LogDto;
//import com.ltzk.admin.service.entity.system.Log;
//import com.ltzk.admin.service.mapper.system.LogMapper;
//import com.ltzk.core.annotation.Logs;
//import com.ltzk.core.baseservice.BaseServiceImpl;
//import com.ltzk.core.utils.BaseContextKit;
//import com.ltzk.core.utils.Checker;
//import com.ltzk.core.utils.HttpContextUtils;
//import com.ltzk.core.utils.RequestUtils;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.reflect.MethodSignature;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.servlet.http.HttpServletRequest;
//import java.lang.reflect.Method;
//import java.util.Date;
//
///**
// * <p>
// * 系统日志 服务实现类
// * </p>
// *
// * @author lgs
// * @since 2019-08-12
// */
//@Transactional
//@Service
//public class LogServiceImpl extends BaseServiceImpl<LogDto, Log, LogMapper> implements LogService {
//
//
//    @Override
//    public void saveLog(ProceedingJoinPoint joinPoint, int time) {
//        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
//        Date date = new Date();
//        Method method = signature.getMethod();
//        LogDto sysLog = new LogDto();
//        Logs logs = method.getAnnotation(Logs.class);
//        if (logs != null) {
//            String opera=logs.operaEnum().getCode()+logs.operation();
//            sysLog.setOperation(opera);
//            sysLog.setModule(logs.module().getName());
//            sysLog.setType(logs.module().getCode());
//        }
//        // 请求的方法名
//        String className = joinPoint.getTarget().getClass().getName();
//        String methodName = signature.getName();
//        sysLog.setMethod(className + "." + methodName + "()");
//        // 请求的参数
//        Object[] args = joinPoint.getArgs();
//        try {
//            if(Checker.BeNotEmpty(args)){
//                String params="";
//                for(Object o:args){
//                    if(Checker.BeNotNull(o) && Checker.BeNotBlank(o.toString())){
//                        params += JSON.toJSONString(o)+"|";
//                    }
//                }
//                if(Checker.BeNotBlank(params)&&params.endsWith("|"))
//                params=params.substring(0,params.length()-1);
//                sysLog.setParams(params);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        // 获取request
//        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
//        // 设置IP地址
//        sysLog.setIp(RequestUtils.getIpAddr(request));
//        // 用户名
//        String userId = BaseContextKit.getUserId();
//        String userName = BaseContextKit.getUserName();
//        String name = BaseContextKit.getName();
//        if (Checker.BeNotBlank(userId)) {
//            sysLog.setUserId(userId);
//        }
//        if (Checker.BeNotBlank(userName)) {
//            sysLog.setUsername(userName);
//        }
//        if (Checker.BeNotBlank(name)) {
//            sysLog.setName(name);
//        }
//        sysLog.setTime(time).setId(generateId());
//        insert(sysLog);
//    }
//
//    @Transactional
//    @Override
//    public void deleteLogBeforeMonth(Integer month) {
//        if(month!=null){
//            Date date = DateUtil.date();
//            Date newDate = DateUtil.offset(date, DateField.MONTH, -month);
//            QueryWrapper<Log> queryWrapper=new QueryWrapper();
//            queryWrapper.lt("gmt_create",newDate);
//            baseMapper.delete(queryWrapper);
//        }
//    }
//
//}
