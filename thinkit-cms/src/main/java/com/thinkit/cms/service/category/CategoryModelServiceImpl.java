package com.thinkit.cms.service.category;

import com.thinkit.cms.api.category.CategoryModelService;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.dto.category.CategoryModelDto;
import com.thinkit.cms.entity.category.CategoryModel;
import com.thinkit.cms.mapper.category.CategoryModelMapper;
import com.thinkit.core.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 分类扩展模型 服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-08-08
 */
@Transactional
@Service
public class CategoryModelServiceImpl extends BaseServiceImpl<CategoryModelDto, CategoryModel, CategoryModelMapper> implements CategoryModelService {

    @Autowired
    CategoryService categoryService;


    @Transactional
    @Override
    public boolean delete(String id) {
        categoryService.deleteCategoryModel(id);
        return super.deleteByPk(id);
    }

    @Override
    public List<CategoryModelDto> loadModels(CategoryModelDto v) {
        return baseMapper.loadModels(v);
    }

    @Override
    public String getDesignField(String id) {
        return baseMapper.getDesignField(id);
    }
}
