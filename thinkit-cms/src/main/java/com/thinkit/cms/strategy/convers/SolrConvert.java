package com.thinkit.cms.strategy.convers;

import com.google.common.collect.Lists;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.strategy.filter.TmpActuator;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.utils.Checker;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SolrConvert {


    public Object convertParam(Object params, ActionEnum actionEnum, TmpActuator actuator){
        if(Checker.BeNotNull(params)){
            if(ActionEnum.CREATE_CONTENT.equals(actionEnum) ||
                ActionEnum.UPDATE_CONTENT.equals(actionEnum) ||
                ActionEnum.TOP_CONTENT.equals(actionEnum)){ // 创建,更新,置顶
                ContentDto content = (ContentDto) params;
                return content;
            }else if(ActionEnum.PUBLISH_CONTENT.equals(actionEnum)){ //发布
                Object object = actuator.getChecker().getParam();
                if(Checker.BeNotNull(object) && object instanceof List){
                    List<String> cids =  (List) object;
                    return Checker.BeNotEmpty(cids)?cids: Lists.newArrayList();
                }
            }else if(ActionEnum.DELETE_CONTENT.equals(actionEnum)){ //删除
                Object object = actuator.getChecker().getParam();
                if(Checker.BeNotNull(object) && object instanceof ContentDto){
                   return  object;
                }
            }
        }
        return null;
    }

}
