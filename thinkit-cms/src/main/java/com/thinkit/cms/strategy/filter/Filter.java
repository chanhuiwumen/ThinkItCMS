package com.thinkit.cms.strategy.filter;

import com.thinkit.cms.dto.strategy.StrategyDto;
import com.thinkit.cms.strategy.checker.ActuatorCheck;

/**
 * @author lg
 * 责任链处理 特效
 */
public interface Filter {

    /**
     * 模板处理器处理
     * @param params 参数
     * @param chain 过滤器链条
     */
    void doFilter(Object params, FilterChain chain);


    /**
     * 设置检查器
     * @param actuatorCheck
     */
    void setChecker(ActuatorCheck actuatorCheck);



     ActuatorCheck getChecker();


    Filter setStrategy(StrategyDto strategy);

    String triggerTime();

    Object getParam();
}
