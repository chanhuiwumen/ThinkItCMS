package com.thinkit.cms.controller.fragment;

import com.thinkit.cms.api.fragment.FragmentAttrService;
import com.thinkit.cms.dto.fragment.FragmentAttrDto;
import com.thinkit.core.annotation.SiteMark;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 页面片段数据表 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-03
 */
@Validated
@RestController
@RequestMapping("fragmentAttr")
public class FragmentAttrController extends BaseController<FragmentAttrService> {


    @GetMapping("getDesignAttrById")
    public Map<String,Object> getDesignAttrById(@NotBlank @RequestParam String id){
       return  service.getDesignAttrById(id);
    }


    @PostMapping(value="insertFragmentAttr")
    public void insertFragmentAttr(@RequestBody FragmentAttrDto fragmentAttrDto ){
        service.insertFragmentAttr(fragmentAttrDto);
    }

    @PostMapping(value="updateFragmentAttr")
    public void updateFragmentAttr(@RequestBody FragmentAttrDto fragmentAttrDto ){
        service.updateFragmentAttr(fragmentAttrDto);
    }

    @PutMapping("update")
    public void update(@RequestBody FragmentAttrDto v){
        service.updateByPk(v);
    }

    @DeleteMapping("delFragmentAttr")
    public boolean delFragmentAttr(@NotBlank @RequestParam String id) {
         return service.deleteByPk(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<FragmentAttrDto> list(@RequestBody FragmentAttrDto v){
        return service.listDto(v);
    }

    @SiteMark
    @PostMapping("page")
    public PageDto<FragmentAttrDto> listPage(@RequestBody PageDto<FragmentAttrDto> pageDto){
        return service.listPage(pageDto);
    }

}
