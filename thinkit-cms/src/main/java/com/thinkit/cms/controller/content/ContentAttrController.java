package com.thinkit.cms.controller.content;

import com.thinkit.cms.api.content.ContentAttrService;
import com.thinkit.cms.dto.content.ContentAttrDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 内容扩展 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
@Validated
@RestController
@RequestMapping("contentAttr")
public class ContentAttrController extends BaseController<ContentAttrService> {

    @GetMapping("getByPk")
    public ContentAttrDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @PostMapping(value="save")
    public void save(@Validated @RequestBody ContentAttrDto v){
        service.insert(v);
    }

    @PutMapping("update")
    public void update(@RequestBody ContentAttrDto v){
        service.updateByPk(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteByPk(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<ContentAttrDto> list(@RequestBody ContentAttrDto v){
        return service.listDto(v);
    }

    @PostMapping("page")
    public PageDto<ContentAttrDto> listPage(@RequestBody PageDto<ContentAttrDto> pageDto){
        return service.listPage(pageDto);
    }

}
