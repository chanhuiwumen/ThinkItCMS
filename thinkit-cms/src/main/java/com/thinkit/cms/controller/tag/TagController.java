package com.thinkit.cms.controller.tag;
import com.thinkit.cms.api.tag.TagService;
import com.thinkit.cms.dto.tag.TagDto;
import com.thinkit.core.annotation.SiteMark;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 标签 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-20
 */
@Validated
@RestController
@RequestMapping("tag")
public class TagController extends BaseController<TagService> {


    @GetMapping("getByPk")
    public TagDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @SiteMark
    @PostMapping(value="save")
    public void save(@Validated @RequestBody TagDto v){
        service.insert(v);
    }

    @SiteMark
    @PutMapping("update")
    public void update(@RequestBody TagDto v){
        service.updateByPk(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteByPk(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<TagDto> list(@RequestBody TagDto v){
        return service.listDto(v);
    }

    @SiteMark
    @PostMapping("page")
    public PageDto<TagDto> listPage(@RequestBody PageDto<TagDto> pageDto){
        return service.listPage(pageDto);
    }

}
