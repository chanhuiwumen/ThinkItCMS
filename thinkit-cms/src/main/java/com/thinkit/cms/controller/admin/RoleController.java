package com.thinkit.cms.controller.admin;

import com.thinkit.cms.api.admin.RoleService;
import com.thinkit.cms.dto.admin.RoleDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.PageDto;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author DL
 * @since 2018-03-19
 */
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController<RoleService> {


    @RequestMapping("page")
    public PageDto<RoleDto> page(@RequestBody PageDto<RoleDto> roleDto) {
        if(!"1".equals(getUserId())){
            roleDto.getDto().condition().notIn("id","1");
        }
        return service.listPage(roleDto);
    }


    @Logs(module = LogModule.ROLE,operation = "创建角色")
    @PostMapping("/save")
    public boolean save(@RequestBody RoleDto role) {
        return service.insert(role);
    }

    @Logs(module = LogModule.ROLE,operation = "编辑角色")
    @PostMapping("/update")
    public boolean update(@RequestBody @Valid RoleDto role) {
        return service.updateByPk(role);
    }


    @Logs(module = LogModule.ROLE,operation = "删除角色")
    @DeleteMapping("/delete")
    public ApiResult deleteRow(@RequestParam String id) {
        return service.deleteById(id);
    }

    /**
     * @param @return
     * @return List<Role>
     * @throws
     * @Description: 添加用户时需要选择用户角色此处查询所有角色
     */
    @GetMapping("/selectAllRoles")
    public List<RoleDto> selectAllRoles() {
        return service.selectAllRoles();
    }


}
