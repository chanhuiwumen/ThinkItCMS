package com.thinkit.cms.controller.site;

import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.dto.admin.OrgDto;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.core.annotation.NotDecorate;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.model.Tree;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 站点管理表 前端控制器
 * </p>
 *
 * @author LG
 * @since 2020-07-22
 */
@Validated
@RestController
@RequestMapping("site")
public class SiteController extends BaseController<SiteService> {


    @GetMapping("getByPk")
    public SiteDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @PostMapping(value="save")
    public void save(@Validated @RequestBody SiteDto v){
        service.save(v);
    }

    @PutMapping("update")
    public void update(@RequestBody SiteDto v){
        service.update(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.delete(id);
    }


    @PutMapping("setDefault")
    public void setDefault(@NotBlank(message = "站点ID不能为空")@RequestParam("siteId") String siteId){
        service.setDefault(siteId);
    }


    /**
     * 设置为组织默认
     * @param siteId
     */
    @PutMapping("setOrgDefault")
    public void setOrgDefault(@NotBlank(message = "站点ID不能为空")@RequestParam("siteId") String siteId){
        service.setOrgDefault(siteId);
    }



    @PostMapping("list")
    public  List<SiteDto> list(@RequestBody SiteDto v){
        return service.listDto(v);
    }

    // @SiteMark(type=SiteOperation.READ)
    @PostMapping("getAllSite")
    public  List<SiteDto> getAllSite(@RequestBody SiteDto v){
        return service.getAllSite(v,getUserId(),getOrgId());
    }

    @PostMapping("page")
    public PageDto<SiteDto> listPage(@RequestBody PageDto<SiteDto> pageDto){
        long startTime=System.currentTimeMillis();            //获得当前时间
         PageDto<SiteDto> pageData = service.listPage(pageDto);
        long endTime=System.currentTimeMillis();                //获得当前时间
        System.out.println("最终执行时间为："+((endTime-startTime)/1000)+"秒");
        return pageData;
    }

    @NotDecorate
    @GetMapping("getDefault")
    public String getDefault(){
        return service.getDefault();
    }

    @GetMapping("getOrgTres")
    public Tree<OrgDto> getOrgTres(){
        return  service.getOrgTres();
    }

}
