package com.thinkit.cms.controller.category;

import com.thinkit.cms.api.category.CategoryTemplateService;
import com.thinkit.cms.dto.category.CategoryTemplateDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 分类-模板配置表 前端控制器
 * </p>
 *
 * @author LG
 * @since 2020-08-12
 */
@Validated
@RestController
@RequestMapping("api/categoryTemplate")
public class CategoryTemplateController extends BaseController<CategoryTemplateService> {


    @GetMapping("getByPk")
    public CategoryTemplateDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @PostMapping(value="save")
    public void save(@Validated @RequestBody CategoryTemplateDto v){
        service.insert(v);
    }

    @PutMapping("update")
    public void update(@RequestBody CategoryTemplateDto v){
        service.updateByPk(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteByPk(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<CategoryTemplateDto> list(@RequestBody CategoryTemplateDto v){
        return service.listDto(v);
    }

    @PostMapping("page")
    public PageDto<CategoryTemplateDto> listPage(@RequestBody PageDto<CategoryTemplateDto> pageDto){
        return service.listPage(pageDto);
    }

}
