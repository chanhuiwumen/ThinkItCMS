package com.thinkit.cms.controller.admin;

import com.thinkit.cms.api.admin.ApiSourceService;
import com.thinkit.cms.dto.admin.ApiSourceDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.model.Tree;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LG
 * @since 2020-04-24
 */
@Validated
@RestController
@RequestMapping("apiSource")
public class ApiSourceController extends BaseController<ApiSourceService> {


    @GetMapping("getByPk")
    public ApiSourceDto get(@NotBlank @RequestParam String id){
       return  service.getById(id);
    }

    @Logs(module = LogModule.ADMIN,operation = "创建接口")
    @PostMapping(value="save")
    public void save(@Validated @RequestBody ApiSourceDto v){
        service.save(v);
    }

    @Logs(module = LogModule.ADMIN,operation = "修改接口")
    @PutMapping("update")
    public void update(@RequestBody ApiSourceDto v){
        service.update(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteById(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<ApiSourceDto> list(@RequestBody ApiSourceDto v){
        return service.listDto(v);
    }

    @PostMapping("page")
    public PageDto<ApiSourceDto> listPage(@RequestBody PageDto<ApiSourceDto> pageDto){
        return service.listPage(pageDto);
    }

    @RequestMapping("/selectTreeList")
    public Tree<ApiSourceDto> selectTreeList() {
        Tree<ApiSourceDto> menus = service.selectTreeList();
        return menus;
    }

    @RequestMapping("/selectTreeSourceByCli")
    public Tree<ApiSourceDto> selectTreeMenuByRole(@RequestParam String clientId) {
        Tree<ApiSourceDto> resources = service.selectTreeResourceByClientId(clientId);
        return resources;
    }
}
