package com.thinkit.cms.controller.admin;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.model.LogDto;
import com.thinkit.log.service.LogService;
import com.thinkit.utils.model.PageDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统日志 前端控制器
 * </p>
 *
 * @author lgs
 * @since 2019-08-12
 */
@RestController
@RequestMapping("log")
public class LogController extends BaseController<LogService> {

    @RequestMapping(value = "/page",method = RequestMethod.POST)
    public PageDto<LogDto> listPage(@RequestBody PageDto<LogDto> dto){
        LogDto logDto = dto.getDto();
        logDto.condition().orderByDesc("gmt_create").like("user_account",logDto.getUserAccount()).
        like("type",logDto.getType()).eq("ip",logDto.getIp());
        return service.listPage(dto);
    }

    @RequestMapping(value = "/pageForMoint",method = RequestMethod.POST)
    public PageDto<LogDto> pageForMoint(@RequestBody PageDto<LogDto> dto){
        dto.setPageSize(6);
        LogDto logDto = dto.getDto();
        logDto.condition().orderByDesc("gmt_create").like("user_account",logDto.getUserAccount()).
                like("type",logDto.getType()).eq("ip",logDto.getIp()).eq("user_id",getUserId());
        return service.listPage(dto);
    }
}
