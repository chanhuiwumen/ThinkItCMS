package com.thinkit.cms.controller.model;

import com.thinkit.cms.api.model.ModelService;
import com.thinkit.cms.dto.model.FieldDto;
import com.thinkit.cms.dto.model.ModelDto;
import com.thinkit.core.annotation.valid.ValidGroup1;
import com.thinkit.core.annotation.valid.ValidGroup2;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.utils.enums.BelongEnum;
import com.thinkit.utils.model.DynamicModel;
import com.thinkit.utils.model.KeyValueModel;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.utils.ModelFieldUtil;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 站点管理表 前端控制器
 * </p>
 *
 * @author LG
 * @since 2020-07-22
 */
@Validated
@RestController
@RequestMapping("model")
public class ModelController extends BaseController<ModelService> {

    @GetMapping("loadModel")
    public List<DynamicModel> loadModel(){
        return ModelFieldUtil.loadModel(BelongEnum.MODEL);
    }

    @GetMapping("loadTemplate")
    public Map<String,Object> loadTemplateTree(){
        return service.loadTemplate();
    }

    @RequestMapping("getByPk")
    protected FieldDto getByPk(@NotBlank(message = "主键不能为空")  String pk){
        return service.getById(pk);
    }

    @Logs(module = LogModule.MODEL,operation = "查看模型列表")
    @RequestMapping(value = "/page",method = RequestMethod.POST)
    public PageDto<FieldDto> listPage(@RequestBody PageDto<FieldDto> dto){
        return service.listPage(dto);
    }


    @GetMapping("listModel")
    public List<ModelDto> listModel(@RequestParam String categoryId){
       return service.listModel(categoryId);
    }

    @Logs(module = LogModule.MODEL,operation = "创建模型")
    @PostMapping(value="save")
    public void saveModel(@Validated(value = {ValidGroup1.class}) @RequestBody FieldDto v){
        service.saveModel(v);
    }

    @Logs(module = LogModule.MODEL,operation = "删除模型")
    @RequestMapping(value="delete",method = RequestMethod.DELETE)
    public void delete(@NotBlank String id){
        service.deleteByPk(id);
    }

    @Logs(module = LogModule.MODEL,operation = "创建模型")
    @PutMapping(value="update")
    public void updateModel(@Validated(value = {ValidGroup2.class}) @RequestBody FieldDto v){
        service.updateModel(v);
    }

    /**
     * 获取发布模型
     * @param categoryId
     * @return
     */
    @GetMapping("listPublihModel")
    public  List<KeyValueModel> listPublihModel(@NotBlank String categoryId){
        return service.listPublihModel(categoryId);
    }
}
