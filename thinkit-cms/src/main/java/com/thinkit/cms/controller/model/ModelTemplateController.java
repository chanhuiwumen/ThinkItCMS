package com.thinkit.cms.controller.model;

import com.thinkit.cms.api.model.ModelTemplateService;
import com.thinkit.cms.dto.model.ModelTemplateDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 模型模板针对不同的站点不同的模型下有不同的 模板页面 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
@Validated
@RestController
@RequestMapping("modelTemplate")
public class ModelTemplateController extends BaseController<ModelTemplateService> {


    @GetMapping("getByPk")
    public ModelTemplateDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @PostMapping(value="save")
    public void save(@Validated @RequestBody ModelTemplateDto v){
        service.insert(v);
    }

    @PutMapping("update")
    public void update(@RequestBody ModelTemplateDto v){
        service.updateByPk(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteByPk(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<ModelTemplateDto> list(@RequestBody ModelTemplateDto v){
        return service.listDto(v);
    }

    @PostMapping("page")
    public PageDto<ModelTemplateDto> listPage(@RequestBody PageDto<ModelTemplateDto> pageDto){
        return service.listPage(pageDto);
    }

}
