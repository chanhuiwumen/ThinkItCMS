package com.thinkit.cms.directives.directive;

import com.thinkit.cms.api.fragment.FragmentAttrService;
import com.thinkit.directive.emums.DirectiveEnum;
import com.thinkit.directive.render.BaseDirective;
import com.thinkit.directive.render.BaserRender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component
public class FragmentDataDirective extends BaseDirective {

    @Autowired
    FragmentAttrService attrService;

    @Override
    public void execute(BaserRender render) throws IOException, Exception {
        String code=render.getString("code");
        List<Map> fragmentDatas=attrService.listDataByCode(code);
        render.put(getName().getCode(),fragmentDatas).render();
    }

    @Override
    public DirectiveEnum getName() {
        return DirectiveEnum.FRAGMENT_DATA;
    }
}
