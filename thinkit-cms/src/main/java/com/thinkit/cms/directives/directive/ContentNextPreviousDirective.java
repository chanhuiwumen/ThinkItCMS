package com.thinkit.cms.directives.directive;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.directive.emums.DirectiveEnum;
import com.thinkit.directive.render.BaseDirective;
import com.thinkit.directive.render.BaserRender;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * 获取上一篇、下一篇
 */
@Component
public class ContentNextPreviousDirective extends BaseDirective {

    @Autowired
    ContentService contentService;

    @Override
    public void execute(BaserRender render) throws IOException, Exception {
        String id=render.getString("id");
        String categoryId=render.getString("categoryId");
        if(Checker.BeNotBlank(id)){
            Map<String,Object> content = contentService.nextPrevious(id,categoryId);
            render.putAll(content).render();
        }
    }

    @Override
    public DirectiveEnum getName() {
        return DirectiveEnum.CMS_CATEGORY_CONTENG_NEXTPREVIOUS;
    }
}
