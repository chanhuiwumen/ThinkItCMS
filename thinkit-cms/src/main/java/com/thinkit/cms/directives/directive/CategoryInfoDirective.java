package com.thinkit.cms.directives.directive;

import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.directive.emums.DirectiveEnum;
import com.thinkit.directive.render.BaseDirective;
import com.thinkit.directive.render.BaserRender;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * 获取栏目详情
 */
@Component
public class CategoryInfoDirective extends BaseDirective {

    @Autowired
    CategoryService categoryService;

    @Override
    public void execute(BaserRender render) throws IOException, Exception {
        String code=render.getString("code");
        String id=render.getString("id");
        if(Checker.BeNotBlank(code) || Checker.BeNotBlank(id)){
            Map<String,Object> category = categoryService.info(code,id);
            render.putAll(category).render();
        }
    }

    @Override
    public DirectiveEnum getName() {
        return DirectiveEnum.CMS_CATEGORY_DIRECTIVE;
    }
}
