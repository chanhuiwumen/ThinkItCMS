package com.thinkit.cms.directives.directive;

import com.thinkit.cms.api.site.SiteService;
import com.thinkit.directive.emums.DirectiveEnum;
import com.thinkit.directive.render.BaseDirective;
import com.thinkit.directive.render.BaserRender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class SiteDirective extends BaseDirective {

    @Autowired
    SiteService siteService;

    @Override
    public void execute(BaserRender render) throws IOException, Exception {
        Map<String,Object> siteInfo =siteService.direForSiteInfo();
        render.putAll(siteInfo).render();
    }

    @Override
    public DirectiveEnum getName() {
        return DirectiveEnum.SITE;
    }
}
