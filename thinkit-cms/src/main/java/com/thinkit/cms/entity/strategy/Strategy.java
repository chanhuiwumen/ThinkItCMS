package com.thinkit.cms.entity.strategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;
/**
 * <p>
 * 
 * </p>
 *
 * @author lg
 * @since 2021-05-12
 */
@Data
@Accessors(chain = true)
@TableName("tk_strategy")
public class Strategy extends BaseModel {

    /**
     * 动作编码
     */

        @TableField("action_code")
        private String actionCode;


    /**
     * 动作名称
     */

        @TableField("action_name")
        private String actionName;


    /**
     * 执行器编码
     */

        @TableField("actuator_code")
        private String actuatorCode;


    /**
     * 执行器名称
     */

        @TableField("actuator_name")
        private String actuatorName;


    /**
     * 栏目ID
     */

        @TableField("category_id")
        private String categoryId;


    /**
     * 执行时间
     */

        @TableField("trigger_time")
        private String triggerTime;


    /**
     * 顺序
     */

        @TableField("order_num")
        private Integer orderNum;



        @TableField("is_start")
        private Boolean isStart;

}
