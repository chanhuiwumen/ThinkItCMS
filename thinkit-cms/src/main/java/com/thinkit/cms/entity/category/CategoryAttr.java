package com.thinkit.cms.entity.category;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;
/**
 * <p>
 * 分类扩展
 * </p>
 *
 * @author lg
 * @since 2020-08-10
 */
@Data
@Accessors(chain = true)
@TableName("tk_category_attr")
public class CategoryAttr extends BaseModel {

    /**
     * 分类ID
     */

        @TableField("category_id")
        private String categoryId;


    /**
     * 标题(分类标题用于 SEO 关键字优化)
     */

        @TableField("title")
        private String title;


    /**
     * 关键词用于 SEO 关键字优化
     */

        @TableField("keywords")
        private String keywords;


    /**
     * 描述用于 SEO 关键字优化
     */

        @TableField("description")
        private String description;


    /**
     * 数据JSON
     */

        @TableField("data")
        private String data;


    /**
     * 创建用户id
     */



    /**
     * 修改人id
     */



    /**
     * 创建时间
     */



    /**
     * 修改时间
     */






}
