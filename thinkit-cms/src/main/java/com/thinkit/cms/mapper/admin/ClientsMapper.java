package com.thinkit.cms.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.admin.ClientsDto;
import com.thinkit.cms.entity.admin.Clients;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 终端信息表 Mapper 接口
 * </p>
 *
 * @author LG
 * @since 2020-04-24
 */
@Mapper
public interface ClientsMapper extends BaseMapper<Clients> {

    List<ClientsDto> listClients(@Param("exclude") String exclude);

    IPage<ClientsDto> listPage(IPage<ClientsDto> pages, @Param("dto") ClientsDto dto);

    ClientsDto getByPk(@Param("pk") Serializable pk);

    void updateSecretByClientId(@Param("clientId") String clientId,
                                @Param("clientSecret") String clientSecret,
                                @Param("accessTokenValidity") Integer accessTokenValidity);
}
