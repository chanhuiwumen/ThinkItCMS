package com.thinkit.cms.mapper.category;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.category.CategoryModelDto;
import com.thinkit.cms.entity.category.CategoryModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 分类扩展模型 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-08
 */
@Mapper
public interface CategoryModelMapper extends BaseMapper<CategoryModel> {

    List<CategoryModelDto> loadModels(@Param("dto") CategoryModelDto v);

    String getDesignField(@Param("id") String id);
}
