package com.thinkit.cms.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.entity.admin.ApiSource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LG
 * @since 2020-04-24
 */
@Mapper
public interface ApiSourceMapper extends BaseMapper<ApiSource> {

    List<String> selectAppPermsByUrl(@Param("url") String url, @Param("application") String application);

    Set<String> selectAppPermsByClient(@Param("clientId") String clientId, @Param("application") String application);

    Integer checkerHasResource(@Param("application") String application,
                               @Param("url") String url,
                               @Param("perm") String perm,
                               @Param("id") String id);
}
