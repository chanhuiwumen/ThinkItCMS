package com.thinkit.cms.mapper.category;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.category.CategoryAttrDto;
import com.thinkit.cms.entity.category.CategoryAttr;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 分类扩展 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-10
 */
@Mapper
public interface CategoryAttrMapper extends BaseMapper<CategoryAttr> {

    void updateByCategory(@Param("categoryId") String categoryId, @Param("data") String attrData);

    void updateSeoByCategoryId(@Param("categoryId") String categoryId,
                               @Param("title") String title,
                               @Param("keywords") String keywords,
                               @Param("description") String description);

    CategoryAttrDto getSeo(@Param("categoryId") String categoryId);
}
