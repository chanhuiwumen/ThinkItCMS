package com.thinkit.cms.mapper.template;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.template.TemplateDto;
import com.thinkit.cms.entity.template.Template;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 模板表 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-07-23
 */
@Mapper
public interface TemplateMapper extends BaseMapper<Template> {

    Integer hashTempCode(@Param("id") String id, @Param("code") String code);

    /**
     * 获取模板位置
     * @param id
     * @return
     */
    String getTempLoaction(@Param("root")String root,@Param("id") String id);

    IPage<TemplateDto> listPage(IPage<TemplateDto> pages, @Param("dto") TemplateDto dto);

    Integer getStartTemplate(@Param("siteId") String siteId, @Param("templateId") String templateId);

    void deleteByCode(@Param("templateCode") String templateCode);

    TemplateDto findTempConfig( @Param("templateId")  String templateId);
}
