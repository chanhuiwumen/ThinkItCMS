package com.thinkit.cms.mapper.fragment;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.fragment.FragmentModelDto;
import com.thinkit.cms.entity.fragment.FragmentModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 页面片段文件模型 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-02
 */
@Mapper
public interface FragmentModelMapper extends BaseMapper<FragmentModel> {

    Integer ckHasCode(@Param("code") String code , @Param("templateId") String templateId);


    List<FragmentModelDto> fragmentModelByTempId(@Param("templateId") String templateId);

    String getFragmentDesign(@Param("id") String id);

    String getFragmentExtendDesign(@Param("id") String id);

    FragmentModelDto getFragmentBaseByPk(@Param("id")  String id);

    String getFragmentPathByCode(@Param("code") String code);

    String getPartImport(@Param("title") String title);
}
