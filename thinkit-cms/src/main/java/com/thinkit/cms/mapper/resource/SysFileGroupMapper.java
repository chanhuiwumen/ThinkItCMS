package com.thinkit.cms.mapper.resource;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.resource.SysFileGroupDto;
import com.thinkit.cms.entity.resource.SysFileGroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sysFileGroup
 * @since 2021-07-03
 */
@Mapper
public interface SysFileGroupMapper extends BaseMapper<SysFileGroup> {

    List<String> listGroupIds(@Param("siteId") String siteId);

    List<SysFileGroupDto> listGroupName(@Param("dto") SysFileGroupDto v);

    int hasName(@Param("name") String name, @Param("siteId") String siteId, @Param("id") String id);

    void updateToNoGid(@Param("gid") String gid);

    void updateFileGid(@Param("fileId") String fileId, @Param("gid") String gid);
}
